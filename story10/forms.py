from django import forms
from django.contrib.auth.models import User
from .models import Profile
from django.contrib.auth.forms import UserCreationForm

class SignUpForm(UserCreationForm):
    image = forms.URLField(required=True)

    class Meta:
        model = User
        fields = ('username',
        'first_name', 
        'last_name',
        'email',
        'image',
        'password1', 
        'password2', )