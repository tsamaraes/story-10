from django.urls import path
from . import views

app_name = 'story10'

urlpatterns = [
    path('', views.index, name='index'),
    path('log-in', views.log_in, name='log-in'),
    path('log-out', views.log_out, name='log-out'),
    path('sign-up', views.sign_up, name='sign-up'),
]