from django.shortcuts import render, redirect
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.contrib.auth.forms import UserCreationForm
from .forms import SignUpForm

# Create your views here.
def index(request):
    return render(request, 'index.html')

def sign_up(request):
    if request.method == "POST":
        form = SignUpForm(request.POST, request.FILES)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.profileImage = form.cleaned_data.get('image')
            user.save()
            return redirect('/log-in')
    else:
        form = SignUpForm()
    return render(request, 'sign-up.html', {'form' : form})

def log_in(request):
    if request.method == "POST":
        username = request.POST['username']
        password = request.POST['passworddd']
        user = authenticate(request, username=username, password=password)
        if user is not None:
            login(request, user)
            # Redirect to a success page.
            return redirect('/')
        else:
            # Return an 'invalid login' error message.
            message = {'message':'username or password is invalid!'}
            return render(request, 'log-in.html', message)
    else:
        return render(request, 'log-in.html')

def log_out(request):
    request.session.flush()
    logout(request)
    return redirect('/')