from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
import time

# Create your tests here.
class Story10UnitTest(TestCase):
    def test_url_template_function(self):
        response = Client().get('')
        found = resolve('/')
        self.assertEqual(response.status_code, 200)
        self.assertTemplateUsed(response, 'index.html')
        self.assertEqual(found.func, index)

class Story10FunctionalTest(LiveServerTestCase):
    def setUp(self):
        # super().setUp()
        # chrome_options = webdriver.ChromeOptions()
        # self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.driver = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story10FunctionalTest, self).setUp()

    def tearDown(self):
        self.driver.quit()
        super().tearDown()

    def test_sign_up_then_log_in_then_log_out_then_log_in_with_wrong_password(self):
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        # landing page test
        self.assertIn("Hello There", response_page)

        self.driver.find_element_by_name("signup").click()
        time.sleep(5)
        
        # test for sign up page
        username = 'testaccount'
        pw = '123cheeseburger'

        self.driver.find_element_by_id('id_username').send_keys(username)
        time.sleep(3)
        self.driver.find_element_by_id('id_first_name').send_keys("First Name")
        time.sleep(3)
        self.driver.find_element_by_id('id_last_name').send_keys("Last_Name")
        time.sleep(3)
        self.driver.find_element_by_id('id_email').send_keys("abc@mail.com")
        time.sleep(3)
        self.driver.find_element_by_id('id_image').send_keys("https://picsum.photos/id/237/200/300")
        time.sleep(3)
        self.driver.find_element_by_id('id_password1').send_keys(pw)
        time.sleep(3)
        self.driver.find_element_by_id('id_password2').send_keys(pw)
        time.sleep(3)
        self.driver.find_element_by_name('sign-me-up').click()
        time.sleep(5)

        # test for log in
        self.driver.find_element_by_name('username').send_keys(username)
        time.sleep(3)
        self.driver.find_element_by_name('passworddd').send_keys(pw)
        time.sleep(3)
        self.driver.find_element_by_name('log-in').click()
        time.sleep(3)

        response_page = self.driver.page_source
        self.assertIn('testaccount', response_page)

        # test for log out
        self.driver.find_element_by_name('log-out').click()
        time.sleep(3)

        response_page = self.driver.page_source
        self.assertIn("do you have an account?", response_page)

        self.driver.find_element_by_name('login').click()
        time.sleep(3)

        # test for log in with wrong password
        self.driver.find_element_by_name('username').send_keys(pw)
        time.sleep(3)
        self.driver.find_element_by_name('passworddd').send_keys(username)
        time.sleep(3)
        self.driver.find_element_by_name('log-in').click()
        time.sleep(3)

        response_page = self.driver.page_source
        self.assertIn("username or password is invalid!", response_page)

    def test_sign_up_but_already_have_an_account(self):
        self.driver.get(self.live_server_url)
        response_page = self.driver.page_source

        self.assertIn("Hello There", response_page)

        self.driver.find_element_by_name("signup").click()
        time.sleep(3)

        self.driver.find_element_by_name("go-login").click()
        time.sleep(3)

        response_page = self.driver.page_source
        time.sleep(3)

        self.assertIn("Username", response_page)
        self.assertIn("Password", response_page)
